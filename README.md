# Public Rest API for Delchain (2018-11-13)
# General API Information
* The base endpoint is: **https://testcoreapi.delchain.io/api/v1/** for testing
and **https://coreapi.delchain.io/api/v1/** for Production
* All endpoints return either a JSON object or array.
* HTTP `5XX` return codes are used for internal errors; the issue is on
  Server's side.
  It is important to **NOT** treat this as a failure operation; the execution status is
  **UNKNOWN** and could have been a success.
* HTTP `4XX` returns message as error message 
* Any endpoint can return an ERROR; the error payload is as follows:
```json5
{
  "message": "error message.",
  "data": {
      // Optional payload in case of additional information
  }
}
```

* For `GET` endpoints, parameters must be sent as `query` parameters.
* For `POST`, `PUT` endpoints, the parameters must be sent
  in the `body` with content type
  `application/json`.
* All authenticated api's are requested to accept timestamp parameters apart from their default params

# Endpoint security
## APIKEY
* API-keys are passed into the Rest API via the `APIKEY`
  header.
* All the parameters **are case sensitive**.
* All Apis are secured and can only be accessed with a valid APIKEY in the headers and hmac signature in the payload

## HMAC / Authentication 
* Endpoints require an additional parameter, `hmac`, to be
  sent in the `queryString` for GET or `body` as json for POST and PUT.
* Endpoints use `HMAC SHA256` signatures. The `HMAC SHA256 signature` is a keyed `HMAC SHA256` operation.
  Use your `secretKey` as the key and `payload` as the value for the HMAC operation.
* In case of GET `payload` is defined as all the parameters sent in `queryString`, except the hmac itself,
 arranged in alphabetic order and In case of POST and PUT, all the parameters in `body` converted
 to a string much like queryString in alphabetic order except hmac.
* Working example to calculate hmac is given below.
 

## Timing security
* An endpoint also requires a parameter, `timestamp`, to be sent which
  should be the epochtime( in seconds ) of when the request was created and sent.
* Any authenticated request reaching server with a difference of more than 5 secs, will be rejected.

  

## Working Examples 
### Lets do a  POST /api/v1/account to create an account.
Here is a step-by-step example of how to send a valid payload while hitting the API with 
above endpoint with given apiKey and secretKey as default value.

Key | Value
------------ | ------------
apiKey | 31c261d5-a095-4770-bc15-9ff55e5aa253
secretKey | b64de9f4-c3b2-4721-a1b7-5d17825a3ebb

## In the header of the request
Key | Value
------------ | ------------
APIKEY | 31c261d5-a095-4770-bc15-9ff55e5aa253

##### In the body as json:
Parameter | Value | Required
------------ | ------------ | --------
coin | BTC | YES
account_client_id | a2j88yu9w6 | OPTIONAL
timestamp | 1549349486 | YES ( epoch time in seconds)
hmac | 4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f | YES 

In order to calculate the payload for above parameters we have to convert the key value pairs in query parameters with value of keys in ascending order

* **payload:** account_client_id=a2j88yu9w6&coin=BTC&timestamp=1549349486
* **HMAC SHA256 signature:**
    ```
    hmac: 4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f
    ```
* you can use this online tool for hmac calculation https://www.freeformatter.com/hmac-generator.html 

##### In the query string

* **queryString:** account_client_id=a2j88yu9w6&coin=BTC&timestamp=1549349486&hmac=4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f

This will be converted to the following string:

* **payload:** account_client_id=a2j88yu9w6&coin=BTC&timestamp=1549349486
* **HMAC SHA256 signature:**
    ```
    hmac: 4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f
    ```
* **Resultant query string**
    ```
    account_client_id=a2j88yu9w6&coin=BTC&timestamp=1549349486&hmac=4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f
    ```
# API Endpoints
## Terminology
* `APIKEY` refers to the api_key provided to you from delchain
* `timestamp` refers to epoch time in seconds ( integer )
* `account_client_id` refers to the nickname, that you would like to use when creating an account. by default we will assign an uuidv4
* `client_trade_id` refers to unique id, provided to create an trade. Helps to avoid duplication
* `client_transfer_id` refers to unique id, provided to create an transfer, Helps to avoid duplication
## General endpoints
### Test connectivity
```
GET /api/v1/server
```
Test connectivity to the Rest API.
**Response:**
```json5
{   
    server_time:1549355461,
    status: "good"
}
```

## Authenticated endpoints 
##### Requires timestamp and hmac 
### Accounts Endpoints
#### 1. Create Account 
Will create a new account for given coins.
```
POST /api/v1/accounts
```
**Request**
```json
{   "coin":"BTC",
    "account_client_id": "<your unique key> <optional>",
    "timestamp": "1549349486",
    "hmac": "4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f"
}
```
**Response**
```json5
{
  "message": "success or error message",
  data: {
      "formatted_account_name": "XX",
      id: 1,
      "public_address": "<address>",
      "ticker": "BTC",
      "nickname":"<your account client id>",
      "quantity": "< qty in that address, 0 initially>"
  }
}
```

#### 2. Create transfer request
Will create a new transfer request for coins.
```
POST /api/v1/accounts/transfer
```
**Request**
```json5
{   
    "account_client_id":"u20392",
    source_address: "0x.....232",
    destination_address: "0x...23212",
    "quantity": 10.12,
    "client_transfer_id": "32321",
    "timestamp": "1549349486",
    "hmac": "4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f"
}
```
**Response**
```json5
{
  "message": "success or error message",
  data: {
      "destination_address": "0x...23212",
      "exchange_rate": 102.43,
      "quantity": 10.12,
      "total_usd_value": 1036.59,
  }
}
```

#### 3. Get all transactions
Retrieves blockchain transaction for any account
```
GET /api/v1/accounts/transactions
```
**Request**
```
account_client_id=a2j88yu9w6&address=kj348bkkj&coin=BTC&timestamp=1549349486&hmac=4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f
```

**Response**
```json5
{
  "message": "success or error message",
  data: [{
      "block_height": "50",
      "date_confirmed": "2019-02-05 18:43:54",
      "digital_asset":"Ethereum",
      "txn_hash":"6kyvZBW0yM",
      "value":60
  }],
}
```

#### 4. Get all accounts
Retrieve all your accounts, use offset and limit to paginate.
```
GET /api/v1/accounts/
```
**Request**
```
offset=2&limit=45&timestamp=1549349486&hmac=4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f
```

**Response**
```json5
{
  "message": "success or error message",
  data: [{
      "formatted_account_name": "6efZBWe0edyM (Bitcoin - Warm)",
      "id": "50",
      "public_address": "6efZBWe0edyM",
      "ticker": "BTC",
      "nickname": "Btc Address",
      "quantity": 4.129,
  }],
}
```

#### 5. Get account
Retrieve details of single account.
```
GET /api/v1/accounts/details
```
**Request**
```
account_client_id=a2j88yu9w6&address=kj348bkkj&timestamp=1549349486&hmac=4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f
```

**Response**
```json5
{
  "message": "success or error message",
  data: {
      "formatted_account_name": "6efZBWe0edyM (Bitcoin - Warm)",
      "id": "50",
      "public_address": "6efZBWe0edyM",
      "ticker": "BTC",
      "nickname": "Btc Address",
      "quantity": 4.129,
  },
}
```

#### 6. Get transfer requests
Get details of all transfer request associated with the account. 
```
GET /api/v1/accounts/transfer
```
**Request**
```
account_client_id=a2j88yu9w6&address=kj348bkkj&coin=BTC&timestamp=1549349486&hmac=4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f
```

**Response**
```json5
{
  "message": "success or error message",
  data: [{
      "source_address": "6efZBWe0edyM",
      "destination_address": "6ergWe0jsedyM",
      "exchange_rate": 3201,
      "quantity": 4.129,
      "total_usd_value": 3415,
      "executed": false                         
  }]
}
```

#### 7. Get all transfer requests
Retrieve transfer requests for all the accounts. use offset and limit for pagination
```
GET /api/v1/accounts/transfers
```
**Request**
```
offset=2&limit=45&timestamp=1549349486&hmac=4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f
```

**Response**
```json5
{
  "message": "success or error message",
  data: [{
      "source_address": "6efZBWe0edyM",
      "destination_address": "6ergWe0jsedyM",
      "exchange_rate": 3201,
      "quantity": 4.129,
      "total_usd_value": 3415,     
      "executed": false                                    
  }],
}
```

### Trade Endpoints
#### 1. Create trade request
```
POST /api/v1/trade/
```
**Request**
```json5
{
      "pair": "USDT-USD",
      "type": "MARKET",
      "side": "SELL",                     
      "price": .9967,
      "quantity": 3,
      "client_trade_id": "d233f32a-bb14-4088-a9ab-66e3395a965d",
      "timestamp": "1549349486",
      "hmac": "4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f"
  }
```
**Response**
```json5
{
  "message": "success or error message",
  data: {
      "id": 1,
      "pair": "USDT-USD",
      "type": "MARKET",
      "side": "SELL",                     
      "price": 5.0,
      "quantity": 3.0,
      "client_trade_id": "d233f32a-bb14-4088-a9ab-66e3395a965d",
      "executed": true,
      "date_created": "2019-03-01T07:32:13"                                                                  
  },
}
```

#### 2. Get all trade requests
Get list of all trades, use offset and limit for paging
```
GET /api/v1/trade
```
**Request**
```
offset=2&limit=45&timestamp=1549349486&hmac=2c2a16b7f2604209ef3c5f74ba6fbf617c13c92461d177866691ae4b213f1c1e
```

**Response**
```json5
{
  "message": "success or error message",
  data: [{
      "id": 1,
      "pair": "USDT-USD",
      "type": "MARKET",
      "side": "SELL",                     
      "price": 5.0,
      "quantity": 3.0,
      "client_trade_id": "d233f32a-bb14-4088-a9ab-66e3395a965d",
      "executed": true,
      "date_created": "2019-03-01T07:32:13"                                            
  }],
}
```

#### 3. Get trade status
Details of particular trade
```
GET /api/v1/trade/status
```
**Request**
```
client_trade_id=d233f32a-bb14-4088-a9ab-66e3395a965d&timestamp=1549349486&hmac=68050f196b211d161baabbcf57b58f46bffffdb536b2d7789f9e29e303e90f9e
```

**Response**
```json5
{
  "message": "success or error message",
  data: {
      "id": 1,
      "pair": "USDT-USD",
      "type": "MARKET",
      "side": "SELL",                     
      "price": 5.0,
      "quantity": 3.0,
      "client_trade_id": "d233f32a-bb14-4088-a9ab-66e3395a965d",
      "executed": true,
      "date_created": "2019-03-01T07:32:13"                                                                  
  },
}
```

### Stats Endpoints
#### 1. Get sell/buy prices of ticker
```
GET /api/v1/stats/ticker/:symbol
```
**Request**
```

symbol: "USDT-USD"

```

**Response**
```json5
{
  "message": "success or error message",
  data: {
      "buy_price": 0.99906,
      "sell_price": 0.994107                                                                 
  },
}
```